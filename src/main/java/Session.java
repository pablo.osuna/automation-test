import com.beust.ah.A;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;
import java.util.concurrent.TimeUnit;


public class Session{

    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
      // System.setProperty("webdriver.chrome.driver","/home/innovacion/Descargas/driver/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--whitelisted-ips=''");

        Actions actions = new Actions(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://qa-internal-wms.g-global.io/");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        driver.navigate().refresh();
        driver.manage().window().maximize();

        WebElement txtEmail = driver.findElement(By.name("email"));
        txtEmail.sendKeys("pablo.osuna@g-global.com");

        WebElement txtPassword = driver.findElement(By.name("password"));
        txtPassword.sendKeys("gglobal1");

        WebElement Submit = driver.findElement(By.cssSelector("button[type='submit']"));
        Submit.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Company")));
        WebElement Select = driver.findElement(By.id("Company"));
        Select.click();

        WebElement selectCompany = driver.findElement(By.xpath("//li[@data-value='60346bac72208214676e15ce']"));
        selectCompany.click();

        WebElement Next = driver.findElement(By.xpath("//button[@data-cy='next-button']"));
        Next.click();

        driver.get("https://qa-internal-wms.g-global.io/locations");

        WebElement menu = driver.findElement(By.xpath("//*[@id=':ra:']"));
        actions.moveToElement(menu).build().perform();
        menu.click();
    }

}