import dev.failsafe.internal.util.Durations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Suite {

        public static void main(String[] args) throws InterruptedException {

            WebDriver driver = new ChromeDriver();
            System.setProperty("webdriver.chrome.driver","/Users/pabloosuna/Documents/driver/chromedriver");
            // Actions act = new Actions(driver);

            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--whitelisted-ips=''");

            driver.get("https://qa-auth.g-global.io/?env=633207ea9bfe2b1d9c586671");

            driver.navigate().refresh();
            driver.manage().window().maximize();


            WebElement txtMail = driver.findElement(By.name("email"));
            txtMail.sendKeys("pablo.osuna");

            WebElement txtPass = driver.findElement(By.name("password"));
            txtPass.sendKeys("gglobal1");

            WebElement btnSignIn = driver.findElement(By.xpath("//*[data-cy='submit']"));
            btnSignIn.click();

            WebElement btnTheme = driver.findElement(By.xpath("//*[@data-testid='LightModeIcon']"));
            btnTheme.click();

            WebElement btnCrossings = driver.findElement(By.xpath("//a[@href='/c/cruces']"));
            btnCrossings.click();

            WebElement btnCreate = driver.findElement(By.xpath("//*[text()='Abrir cruce']"));
            btnCreate.click();

            //MODAL NUEVO CRUCE

            WebElement liType = driver.findElement(By.xpath("//*[@aria-haspopup='listbox']"));
            liType.click();

            WebElement type = driver.findElement(By.xpath("//*[@data-value='Importacion']"));
            type.click();

            WebElement client = driver.findElement(By.xpath("//input[@type='text']"));
            client.click();
            client.sendKeys("10503");

            WebElement selectClient = driver.findElement(By.id("controlled-autocomplete-client-option-0"));
            selectClient.click();

            WebElement patente = driver.findElement(By.name("patente"));
            patente.sendKeys("1673");

            WebElement aduana = driver.findElement(By.name("aduana"));
            aduana.sendKeys("400");

            WebElement files = driver.findElement(By.xpath("//*[@type='file']"));
            String files1 = "/home/innovacion/Descargas/cruces/sohen/Factura_SFS230750-PT.txt";
            String files2 = "/home/innovacion/Descargas/cruces/sohen/SFS230750-PT.pdf";
            String allFiles = files1 + "\n" + files2;
            files.sendKeys(allFiles);

            WebElement cancel = driver.findElement(By.xpath("//*[text()='Cancelar']"));
          //  cancel.click();

         //   driver.close();

        }
}
