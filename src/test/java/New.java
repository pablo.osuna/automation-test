import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.SQLOutput;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class New {
    WebDriver driver ;
    String baseUrl = "https://qa-auth.g-global.io/?env=633207ea9bfe2b1d9c586671";


    @BeforeTest
    public void prepare() {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver","/Users/pabloosuna/Documents/driver/chromedriver");

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--whitelisted-ips=''");

        System.out.println("Abriendo Google Chrome y entrando a la pagina");
        driver.get(baseUrl);

        driver.navigate().refresh();
        driver.manage().window().maximize();

    }

    @Test (priority = 0)
    public void openSession(){
        System.out.println("Se inicia sesion");

        WebElement txtMail = driver.findElement(By.name("email"));
        txtMail.sendKeys("pablo.osuna");

        WebElement txtPass = driver.findElement(By.name("password"));
        txtPass.sendKeys("gglobal.01");

        WebElement btnSignIn = driver.findElement(By.xpath("//*[@data-cy='submit']"));
        btnSignIn.click();

        WebElement btnTheme = driver.findElement(By.xpath("//*[@data-testid='LightModeIcon']"));
        btnTheme.click();
    }

    @Test (priority = 1)
    public void newCrossing(){

        System.out.println("Se hace clic en el Modulo de Cruces");
        WebElement btnCrossings = driver.findElement(By.xpath("//a[@href='/c/cruces']"));
        btnCrossings.click();

        System.out.println("Se crea nuevo cruce");
        WebElement btnCreate = driver.findElement(By.xpath("//*[text()='Abrir cruce']"));
        btnCreate.click();

        WebElement liType = driver.findElement(By.xpath("//*[@aria-haspopup='listbox']"));
        liType.click();

        WebElement type = driver.findElement(By.xpath("//*[@data-value='Importacion']"));
        type.click();

        WebElement client = driver.findElement(By.xpath("//input[@type='text']"));
        client.click();
        client.sendKeys("10503");

        WebElement selectClient = driver.findElement(By.id("controlled-autocomplete-client-option-0"));
        selectClient.click();

        WebElement patente = driver.findElement(By.name("patente"));
        patente.sendKeys("1673");

        WebElement aduana = driver.findElement(By.name("aduana"));
        aduana.sendKeys("400");

        WebElement files = driver.findElement(By.xpath("//*[@type='file']"));
        String files1 = "/home/innovacion/Descargas/cruces/sohen/Factura_SFS230750-PT.txt";
        String files2 = "/home/innovacion/Descargas/cruces/sohen/SFS230750-PT.pdf";
        String allFiles = files1 + "\n" + files2;
        files.sendKeys(allFiles);
    }

    @AfterTest
    public void aftherTest() throws Exception{
        System.out.println("Se termino el test");;
        driver.close();
    }

}
